<div class="top"></div>
  <div class="header">
    <div class="container">
      <nav class="navbar navbar-oe" role="navigation">
        <div class="nav-container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
              <span class="sr-only">Toggle navigation</span>
              <i class="glyphicon glyphicon-align-justify"></i>
            </button>
            <a class="navbar-brand" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>">
              <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
            </a>
          </div>
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <?php if (!empty($primary_nav)): ?>
              <?php print render($primary_nav); ?>
            <?php endif; ?>
          </div><!-- /.navbar-collapse -->
        </div>
      </nav>
    </div>
  </div>
    <div class="gray-container">
      <div class="container">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
          <!-- Indicators -->
          <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
            <li data-target="#carousel-example-generic" data-slide-to="3"></li>
            <li data-target="#carousel-example-generic" data-slide-to="4"></li>
          </ol>
          <!-- Wrapper for slides -->
          <div class="carousel-inner">
            <div class="item active">
          	  <img src="/sites/all/themes/energy_dashboard/images/oe-slide5.png" alt="" class="slider-img" />
          	</div>
          	<div class="item">
          	  <img src="/sites/all/themes/energy_dashboard/images/oe-slide2.png" alt="" class="slider-img" />
          	</div>
          	<div class="item">
          	  <img src="/sites/all/themes/energy_dashboard/images/oe-slide3.png" alt="" class="slider-img" />
          	</div>
          	<div class="item">
          	  <img src="/sites/all/themes/energy_dashboard/images/oe-slide4.png" alt="" class="slider-img" />
          	</div>
          	<div class="item">
          	  <img src="/sites/all/themes/energy_dashboard/images/oe-slide1.png" alt="" class="slider-img" />
          	</div>
          </div>
          <!-- Controls -->
          <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev"></a>
          <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next"></a>
        </div><!-- end carousel -->
      </div>
    </div>
    <div class="container">
      <div class="row row-bg">
        <div class="col-md-8">
          <?php if (!empty($title)): ?>
            <h1 class="page-header"><?php print $title; ?></h1>
          <?php endif; ?>
            <?php print $messages; ?>
            <?php if (!empty($tabs)): ?>
              <?php print render($tabs); ?>
            <?php endif; ?>
            <?php if (!empty($page['help'])): ?>
              <?php print render($page['help']); ?>
            <?php endif; ?>
            <?php if (!empty($action_links)): ?>
              <ul class="action-links"><?php print render($action_links); ?></ul>
            <?php endif; ?>
          <?php print render($page['content']); ?>
        </div>
        <div class="col-md-4 oe-sidebar">
          <?php print render($page['sidebar']); ?>
        </div>
      </div>
    </div>
  <div class="footer">
    <div class="container">
      <?php print render($page['footer']); ?>
    </div>
  </div>