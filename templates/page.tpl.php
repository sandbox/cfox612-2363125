<div class="top"></div>
  <div class="header">
    <div class="container">
      <nav class="navbar navbar-oe" role="navigation">
	    <div class="nav-container">
	    <!-- Brand and toggle get grouped for better mobile display -->
	      <div class="navbar-header">
	        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
	          <span class="sr-only">Toggle navigation</span>
	          <i class="glyphicon glyphicon-align-justify"></i>
	        </button>
	        <a class="navbar-brand" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>">
	          <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
	        </a>
	      </div>

      	  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      		<?php if (!empty($primary_nav)): ?>
      		<?php print render($primary_nav); ?>
            <?php endif; ?>
          </div><!-- /.navbar-collapse -->
        </div>
      </nav>
    </div>
  </div>
  <div class="gray-container secondary-container">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
        <?php if (!empty($title)): ?>
          <h1 class="page-header"><?php print $title; ?></h1>
        <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <?php if (!empty($breadcrumb)): print $breadcrumb; endif;?>
      </div>
    </div>
    <div class="row row-bg">
      <div class="col-md-8">
        <?php print $messages; ?>
        <?php if (!empty($tabs)): ?>
        <?php print render($tabs); ?>
        <?php endif; ?>
        <?php if (!empty($page['help'])): ?>
        <?php print render($page['help']); ?>
        <?php endif; ?>
        <?php if (!empty($action_links)): ?>
          <ul class="action-links"><?php print render($action_links); ?></ul>
        <?php endif; ?>
        <?php print render($page['content']); ?>
      </div>
      <div class="col-md-4 oe-sidebar">
        <?php print render($page['sidebar']); ?>
      </div>
    </div>
  </div>
    <div class="footer">
      <div class="container">
        <?php print render($page['footer']); ?>
      </div>
    </div>